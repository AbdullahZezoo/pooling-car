import React from "react";

// components

import DriverNavbar from "components/Navbars/DriverNavbar.js";
import DriverSidebar from "components/Sidebar/DriverSideBar.js";
import HeaderStats from "components/Headers/HeaderStats.js";
import FooterAdmin from "components/Footers/FooterAdmin.js";

export default function Driver({ children }) {
  return (
    <>
      <DriverSidebar />
      
      <div className="relative md:ml-64 bg-blueGray-100">
      <DriverNavbar />  
        
        <div className="px-4 md:px-10 py-40 mx-auto w-full -m-24">
          {children}
          <FooterAdmin />
        </div>
      </div>
    </>
  );
}
