# Pooling Car

### Tailwind CSS and NextJS.
- Used NextJS and Tailwind CSS

### Pages


Here are all the page from the project:
- Admin Pages
  - [Home](https://pooling-cars.vercel.app/admin/home)
- Driver Pages
  - [Home](https://pooling-cars.vercel.app/driver/home)
  - [Dashboard](https://pooling-cars.vercel.app/driver/dashboard)
- Auth Pages
  - [AdminSignin](https://pooling-cars.vercel.app/admin/login)
  - [DriverSignin](https://pooling-cars.vercel.app/driver/login)
  - [DriverSignup](https://pooling-cars.vercel.app/driver/register)


## MYSQL Cloud

* Clever Cloud for mysql host
- [CleverCloud](https://www.clever-cloud.com/en/)
- Database Design and Tables inside MySqlDB.txt file