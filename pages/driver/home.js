
import React from "react";
import DriverTab from "components/Tabs/DriverTab.js";

import Driver from "layouts/Driver.js";

export default function Home() {
  
    
    return (
      <>
        <div className="flex flex-wrap mt-4">
          <div className="w-full mb-12 px-4">
           <DriverTab/>
          </div>
        </div>
      </>
    );
  }
  
  Home.layout = Driver;