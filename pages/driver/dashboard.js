import React from "react";


// layout for page

import Driver from "layouts/Driver.js";
import HeaderStats from "components/Headers/HeaderStats";
import CardRides from "components/Cards/CardRides";

export default function dashboard() {
  return (
    <>
       
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
        <HeaderStats />
        <br />
        <br />
          <CardRides />
        </div>
       
      </div>
    </>
  );
}

dashboard.layout = Driver;
