import React from "react";
import {  QueryCache, useQuery } from 'react-query';

// components

import Tab from "components/Tabs/AdminTab.js";

// layout for page

import Admin from "layouts/Admin.js";

const fetcher = () => fetch("http://localhost:3000/api/rides").then(response => response.json());


export default function Tables() {
  const { data: rides, isLoading, error } = useQuery("rides", fetcher)

  
  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
         <Tab/>
        </div>
      </div>
    </>
  );
}

Tables.layout = Admin;
