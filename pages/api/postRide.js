import {mysql_query} from '../../lib/db';

const handler = async (data, res) => {
    // Run your query
    try {
        const values = ["io", "p", "r", 1, , "pending", null]
        const results = await mysql_query('INSERT INTO rides (customer_name, source_location, destination, price, rate, status, driver_id) VALUES (?,?,?,?,?,?,?)', data 
        );
        
        return res.json(results);
    } catch(e) {
        res.status(500).json({ message: e.message });
    }
  
    // Return the results
    return results
  }

  export default handler;