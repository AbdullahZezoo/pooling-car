import {mysql_query} from '../../lib/db';

const handler = async (_, res) => {
    // Run your query
    try {
        const results = await mysql_query('SELECT * FROM rides');
        return res.json(results);
    } catch(e) {
        res.status(500).json({ message: e.message });
    }
  
    
  
    // Return the results
    return results
  }

  export default handler;